#include <iostream>
#include <math.h>

class			Coord3d
{
public:
	Coord3d()
	{}
	Coord3d(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		std::cout << x << ' ' << y << ' ' << z << '\n';
	}

	double GetX()
	{
		return x;
	}

	double GetY()
	{
		return y;
	}

	double GetZ()
	{
		return z;
	}

private:
	double		x = .0;
	double		y = .0;
	double		z = .0;
};

class			Vector3d : public Coord3d
{
public:
	Vector3d()
	{}
	Vector3d(double _x, double _y, double _z) : Coord3d(_x, _y, _z)
	{}

	double GetVectorLength()
	{
		double sum = pow(this->GetX(), 2) + pow(this->GetY(), 2) + pow(this->GetZ(), 2);
		return sqrt(sum);
	}

private:

};

class			Scale3d : public Coord3d
{
public:
	Scale3d()
	{}
	Scale3d(double _x, double _y, double _z) : Coord3d(_x, _y, _z)
	{}


private:

};

int				main()
{
    Vector3d	v(1, 0, -4);

	v.Show();
	std::cout << v.GetVectorLength() << '\n';

	return 0;
}
